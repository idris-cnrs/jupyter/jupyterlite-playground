# Jupyterlite Playground



## Getting started

JupyterLite deployed as a static site Jean Zay JupyterHub deployment as a Playground. 
This is essentially based on [jupyterlite demo](https://github.com/jupyterlite/demo)

## Building static site

First we need to install all requirements in an isolated python environment

```
$ git clone https://gitlab.com/idris-cnrs/jupyter/jupyterlite-playground.git
$ cd jupyterlite-playground
$ pip install -r requirements.txt
```

Then we need to build `jupyter lite` to produce the static assets

```
jupyter lite build
```

This command will create a `dist` folder that contains all the static assets.

## Testing

Once the `dist` folder is created, we can serve the content with any webserver. 

```
$ cd dist
$ python -m http.server
```

Now navigate to [localhost:8000](http://localhost:8000) to see a working JupyterLab 
server in the browser.
